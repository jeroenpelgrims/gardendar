---
name: # Lavender
scientific_name: # Lavender -> Lavandula
hardiness_zone:
  min: 
  max: 
  # temperature https://en.wikipedia.org/wiki/Hardiness_zone
  # https://www.plantmaps.com/interactive-belgium-plant-hardiness-zone-map-celsius.php
ph_range:
  min:
  max: 
  # 0(acidic)-7(neutral)-14(alkaline)
  # https://en.wikipedia.org/wiki/PH#Classification_of_soil_pH_ranges
life_cycle: # annual, biennial, perennial
light: 
  # low, medium, high, very high (https://en.wikiversity.org/wiki/Houseplant_care)
  # https://www.johnson.k-state.edu/lawn-garden/agent-articles/miscellaneous/defining-sun-requirements-for-plants.html
  # https://www.noao.edu/education/QLTkit/ACTIVITY_Documents/Safety/LightLevels_outdoor+indoor.pdf
  # https://living.thebump.com/many-hours-sun-lavender-plant-need-5970.html (In hours of light/day)
plant_period: # august, september, ...
difficulty:
# water:
# soil_type:
# mineral_nutrients:
# Fertilization season?
# When to sow
# fertilization composition (NPK)
# competitor: https://gardenate.com/plant/Celery
---
