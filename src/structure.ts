import * as z from 'zod';

const HardinessZoneSchema = z.number().int().min(1).max(13);
const AciditySchema = z.number().min(0).max(14);
const SourcesSchema = z.array(z.string().url());

export const PlantSchema = z.object({
  name: z.string(),
  scientific_name: z.string(),
  hardiness_zone: z.object({
    min: HardinessZoneSchema,
    max: HardinessZoneSchema
  }),
  ph_range: z.object({
    min: AciditySchema,
    max: AciditySchema
  }),
  life_cycle: z.array(z.union([
    z.literal("annual"),
    z.literal("biennial"),
    z.literal("perennial")
  ])),
  // light: z.null().optional(),
  // plant_period: z.null().optional(),

  sources: z.object({
    hardiness_zone: SourcesSchema.optional(),
    ph_range: SourcesSchema.optional(),
    light: SourcesSchema.optional(),
    life_cycle: SourcesSchema.optional()
  }).nonstrict()
}).nonstrict();

export type Plant = z.infer<typeof PlantSchema>;