import fs from 'fs';
import path from 'path';
import { default as frontMatter } from 'front-matter';
import { PlantSchema } from '../src/structure';

type Plant = {
  name: string;
  scientific_name: string;
}

const dir = "plants";
const files = fs.readdirSync("plants").map(file => path.join(dir, file));
const plants = files.map(file => [file, fs.readFileSync(file).toString()]);

describe("Each plant has the correct structure", () => {
  for (let [file, data] of plants) {
    it(file, () => {
      const plant = frontMatter<Plant>(data).attributes;
      PlantSchema.parse(plant);
    });
  }
});