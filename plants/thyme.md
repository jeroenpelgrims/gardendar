---
name: Thyme
scientific_name: Thymus vulgaris
hardiness_zone:
  min: 5
  max: 9
ph_range:
  min: 6.6
  max: 7.8
life_cycle:
  - perennial
light: Full sun

sources:
  hardiness_zone:
    - https://www.almanac.com/plant/thyme
    - https://www.gardeningknowhow.com/edible/herbs/thyme/growing-creeping-thyme.htm
  ph_range:
    - https://www.gardeningknowhow.com/edible/herbs/thyme/growing-creeping-thyme.htm
    - http://herbgardening.com/growingthyme.htm
  light:
    - http://herbgardening.com/growingthyme.htm
---
