---
name: Lavender
scientific_name: Lavandula angustifolia
hardiness_zone:
  min: 4
  max: 9
ph_range:
  min: 6.5
  max: 8
life_cycle:
  - perennial
light: Full sun
plant_period:
  - october

sources:
  hardiness_zone:
    - https://www.whiteflowerfarm.com/how-to-grow-lavender-care
    - https://www.fiskars.com/en-us/gardening-and-yard-care/ideas-and-how-tos/planting-and-prep/growing-lavender
  ph_range:
    - https://bonnieplants.com/how-to-grow/growing-lavender/
    - http://sunshinelavenderfarm.com/planting-care/
    - https://www.canr.msu.edu/news/growing-lavender-in-michigan-advice-for-a-purple-garden
  light:
    - https://living.thebump.com/many-hours-sun-lavender-plant-need-5970.html
  plant_period:
    - https://homeguides.sfgate.com/time-plant-lavender-66826.html
---
