---
name: Parsnip
scientific_name: Pastinaca sativa
hardiness_zone:
  min: 2
  max: 9
ph_range:
  min: 6.1
  max: 7.3
life_cycle:
  - biennial
light: Full sun

sources:
  hardiness_zone:
    - https://gardenerspath.com/plants/vegetables/grow-parsnips/
  ph_range:
    - https://gardenerspath.com/plants/vegetables/grow-parsnips/
  light:
    - https://gardenerspath.com/plants/vegetables/grow-parsnips/
---
