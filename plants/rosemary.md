---
name: Rosemary
scientific_name: Salvia rosmarinus
hardiness_zone:
  min: 1
  max: 10
ph_range:
  min: 5
  max: 8
life_cycle:
  - perennial
light: Full sun

sources:
  hardiness_zone:
    - https://www.burpee.com/gardenadvicecenter/herbs/rosemary/growing-rosemary-in-your-home-garden/article10282.html
  ph_range:
    - https://garden.org/learn/articles/view/184/
  light:
    - https://garden.org/learn/articles/view/184/
---
