---
name: Tomato
scientific_name: Solanum lycopersicum
hardiness_zone:
  min: 5
  max: 8
ph_range:
  min: 6
  max: 6.8
life_cycle:
  - perennial
light: Full sun
plant_period:
  - june
  - july
  - november

sources:
  hardiness_zone:
    - https://www.fiskars.com/en-us/gardening-and-yard-care/ideas-and-how-tos/harvesting/the-tomato-zone
  ph_range:
    - https://homeguides.sfgate.com/tomatoes-acidloving-plant-54205.html
    - https://homeguides.sfgate.com/tomato-plants-prefer-morning-afternoon-sun-57922.html
  light:
    - https://homeguides.sfgate.com/tomato-plants-prefer-morning-afternoon-sun-57922.html
  life_cycle:
    - https://homeguides.sfgate.com/tomato-plants-annuals-58163.html
---
